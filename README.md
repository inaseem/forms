# Forms

## Aim

The projects aim is to learn to develop and handle complex forms and structure form data using material UI and react-hook-form.

## Technologies/Libraries used

1. React JS
2. react-hook-form
3. Material UI for React
4. React beautiful dnd
5. yup

## Form types

1. Dynamic form [Link](https://naseemali.gitlab.io/forms/#/panel)

   - Fields can be added deleted modified as required.
   - yup Validations.
   - Ability to drag and drop fields to reorder.
   - Variety of form controls [color pickers, textfields, toggle button groups, dynamic sections, drop downs, etc]
   - form data is persisted to store(sort of redux using react-tracked)
   - ability to revert back changes [by clicking cancel button]
   - a clean and cool ui
   - amazing user experience.

2. Static form [Link](https://naseemali.gitlab.io/forms/#/profile)

   - Fixed(number of) fields
   - yup Validations
   - Animations for form step transition.
   - Ability to modify form

### react-hook-form API's used

1. register
2. useForm
3. control, Controller
4. reset
5. trigger
6. handleSubmit
7. useFieldArray (move, remove, append)
8. resolver (yupResolver)
