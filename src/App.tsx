import '@fontsource/montserrat';
import { green, purple } from '@material-ui/core/colors';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListSubheader from '@material-ui/core/ListSubheader';
import { createMuiTheme, createStyles, makeStyles, Theme, ThemeProvider } from '@material-ui/core/styles';
import MailPanel from 'pages/mail/MailPanel';
import StylePanel from 'pages/panel/StylePanel';
import { Provider } from 'providers';
import React from 'react';
import { HashRouter as Router, Link as RouterLink, Route, Switch } from 'react-router-dom';
import Profile from './pages/profile/Profile';

const theme = createMuiTheme({
  typography: {
    fontFamily: 'Montserrat',
  },
  palette: {
    primary: {
      main: purple[500],
      light: purple[50],
    },
    secondary: {
      main: green[500],
    },
  },
  shape: {
    borderRadius: 10,
  },
});

const themeGreen = createMuiTheme({
  typography: {
    fontFamily: 'Montserrat',
  },
  palette: {
    primary: {
      main: 'rgb(64, 204, 121)',
      light: 'rgba(64, 204, 121,0.87)',
    },
    // secondary: {
    //   main: 'rgb(64, 204, 121)',
    // },
    text: {
      //   primary: 'rgb(66, 70, 81)',
      //   primary: '#424651',
    },
    action: {
      active: 'rgba(64, 204, 121,0.54)',
      selected: 'rgba(64, 204, 121,0.18)',
    },
  },
  shape: {
    borderRadius: 5,
  },
  overrides: {
    MuiButton: {
      contained: {
        textTransform: 'none',
        fontWeight: 'bold',
      },
      outlined: {
        textTransform: 'none',
        fontWeight: 'bold',
      },
    },
  },
});

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      maxWidth: 360,
      backgroundColor: theme.palette.background.paper,
    },
  }),
);

const App: React.FC = () => {
  const classes = useStyles();
  return (
    <div style={{ flexGrow: 1 }}>
      <Router>
        <Switch>
          <Route exact path='/'>
            <ThemeProvider theme={theme}>
              <Grid container alignItems='center' justify='center' style={{ height: '100vh' }}>
                <List subheader={<ListSubheader>Forms</ListSubheader>} className={classes.root} dense>
                  <ListItem>
                    <ListItemText id='mail' primary='Email Form' secondary='Form for sending email' />
                    <ListItemSecondaryAction>
                      <Link to='/mail' component={RouterLink}>
                        Link
                      </Link>
                    </ListItemSecondaryAction>
                  </ListItem>
                  <ListItem>
                    <ListItemText
                      id='panel'
                      primary='SidePanel Form'
                      secondary='Form with drag and drop feature and react hook form integration. Uses fieldArray, register, reset, Controller etc'
                    />
                    <ListItemSecondaryAction>
                      <Link to='/panel' component={RouterLink}>
                        Link
                      </Link>
                    </ListItemSecondaryAction>
                  </ListItem>
                  <ListItem>
                    <ListItemText
                      id='profile'
                      primary='Profile'
                      secondary='Simple 3 step form with animations and react-hook form validations.'
                    />
                    <ListItemSecondaryAction>
                      <Link to='/profile' component={RouterLink}>
                        Link
                      </Link>
                    </ListItemSecondaryAction>
                  </ListItem>
                </List>
              </Grid>
            </ThemeProvider>
          </Route>
          <Route path='/profile'>
            <ThemeProvider theme={theme}>
              <Profile />
            </ThemeProvider>
          </Route>
          <Route path='/panel'>
            <ThemeProvider theme={themeGreen}>
              <CssBaseline />
              <Provider>
                <StylePanel />
              </Provider>
            </ThemeProvider>
          </Route>
          <Route path='/mail'>
            <ThemeProvider theme={themeGreen}>
              <CssBaseline />
              <Provider>
                <MailPanel />
              </Provider>
            </ThemeProvider>
          </Route>
        </Switch>
      </Router>
    </div>
  );
};

export default App;
