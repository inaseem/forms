import { Button } from '@material-ui/core';
import React, { ReactNode } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    unselected: {
      color: theme.palette.text.primary,
    },
    selected: {
      color: theme.palette.primary.main,
    },
  }),
);

type GreenButtonProps = {
  selected?: boolean;
  children: ReactNode;
};

const GreenButton = (props: GreenButtonProps): JSX.Element => {
  const classes = useStyles();
  return (
    <>
      <Button className={props.selected ? classes.selected : classes.unselected}>{props.children}</Button>
    </>
  );
};

export default GreenButton;
