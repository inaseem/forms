import { Grid, IconButton } from '@material-ui/core';
import { createStyles, fade, makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import { Add, Delete, Done, Edit, DragIndicator } from '@material-ui/icons';
import { BootstrapInput } from 'components';
import { EditableTF } from 'components/editabletf';
import React from 'react';
import { DragDropContext, Draggable, DraggableProvidedDragHandleProps, Droppable, DropResult } from 'react-beautiful-dnd';
import { useFieldArray, Control } from 'react-hook-form';

/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable  @typescript-eslint/no-explicit-any */

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    subtitle: {
      fontSize: 14,
      fontWeight: 'bold',
      marginTop: theme.spacing(2),
    },
    content: {
      //   marginTop: theme.spacing(3),
    },
    selected: {
      color: theme.palette.primary.main,
    },
    section: {
      marginTop: theme.spacing(3),
    },
    margin16: {
      marginTop: theme.spacing(1),
    },
    colorInput: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    padLeft16: {
      paddingLeft: theme.spacing(1),
    },
    actionsContainer: {
      marginTop: theme.spacing(3),
    },

    bordered: {
      border: `1px solid ${theme.palette.grey[300]}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
  }),
);

type EditableTFProps = {
  text: string;
  editing?: boolean;
  tfRef?: React.Ref<unknown>;
  sfId: number;
  dragHandleProps?: DraggableProvidedDragHandleProps;
  isDragging?: boolean;
  setSections?: React.Dispatch<React.SetStateAction<number[]>>;
  style?: React.CSSProperties;
  onItemAdd?: () => void;
  onDelete?: () => void;
  onItemDelete?: (index: number) => void;
  name?: string;
  defaultValue?: unknown;
  register: () => React.Ref<unknown>;
  control: Control;
  nestIndex: number;
  errors: unknown;
};

const EditableSection = ({
  editing = true,
  dragHandleProps,
  isDragging = false,
  style = {},
  onDelete,
  name,
  defaultValue,
  onItemAdd,
  control,
  nestIndex,
  register,
}: EditableTFProps): JSX.Element => {
  const theme = useTheme();
  const classes = useStyles();
  const [edit, setEdit] = React.useState(editing);
  const { fields, remove, append, move } = useFieldArray({
    control,
    name: `sectionsF5[${nestIndex}].items`,
  });

  const onDragged = (result: DropResult) => {
    const { destination, source } = result;
    if (!destination) return;
    if (destination.droppableId == source.droppableId && destination.index == source.index) return;
    move(source.index, destination.index);
    // console.log(result);
  };
  return (
    <>
      <Grid
        style={{
          display: 'inline-block',
          ...style,
        }}
        container
      >
        <Grid
          container
          alignItems='center'
          justify='space-between'
          //   {...dragHandleProps}
          className={classes.bordered}
          style={{
            backgroundColor: isDragging ? theme.palette.action.selected : '#F8FBFF',
          }}
          item
          md={12}
          sm={12}
          xs={12}
          lg={12}
          xl={12}
        >
          <Grid item md={10} sm={8} lg={10} xs={8} container alignItems='center' justify='flex-start' direction='row'>
            <Grid item {...dragHandleProps} md={1} sm={1} lg={1} xs={1} xl={1} container alignItems='center'>
              <DragIndicator color='action' />
            </Grid>
            <Grid item md={10} sm={10} lg={10} xs={10} xl={10}>
              <BootstrapInput
                autoFocus
                inputRef={register()}
                readOnly={!edit}
                defaultValue={defaultValue}
                fullWidth
                name={name}
                placeholder='Heading'
                style={{ fontWeight: 'bold' }}
                onKeyPress={e => {
                  if (e.key === 'Enter') {
                    e.preventDefault();
                    setEdit(!edit);
                  }
                }}
              />
            </Grid>
          </Grid>
          <Grid item md={2} sm={4} lg={2} xs={4} container justify='flex-end'>
            <Grid item>
              <IconButton
                size='small'
                aria-label='add field'
                component='span'
                onClick={() => {
                  if (edit) return;
                  append({ value: '' });
                  if (onItemAdd) onItemAdd();
                }}
              >
                <Add />
              </IconButton>
            </Grid>
            <Grid item>
              <IconButton
                size='small'
                aria-label='edit section'
                component='span'
                onClick={() => {
                  setEdit(!edit);
                }}
              >
                {edit ? <Done /> : <Edit />}
              </IconButton>
            </Grid>
            <Grid item>
              <IconButton
                size='small'
                aria-label='delete section'
                component='span'
                onClick={() => {
                  if (onDelete) onDelete();
                }}
              >
                <Delete />
              </IconButton>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Grid container>
        <Grid item md={12} sm={12} xs={12} lg={12} xl={12}>
          <DragDropContext onDragEnd={onDragged}>
            <Droppable droppableId='droppable-2'>
              {provided => (
                <div {...provided.droppableProps} ref={provided.innerRef}>
                  {fields.map((item, index) => (
                    <Draggable draggableId={`${item.id}`} index={index} key={item.id}>
                      {(provided, snapshot) => (
                        <div key={item.id} {...provided.draggableProps} ref={provided.innerRef}>
                          <EditableTF
                            // style={{ marginTop: index == 0 ? theme.spacing(1) : theme.spacing(1) }}
                            isDragging={snapshot.isDragging}
                            dragHandleProps={provided.dragHandleProps}
                            text=''
                            tfRef={register()}
                            defaultValue={item.value}
                            name={`sectionsF5[${nestIndex}].items[${index}].value`}
                            key={item.id}
                            tfId={index}
                            onDelete={() => {
                              remove(index);
                            }}
                          />
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {provided.placeholder}
                </div>
              )}
            </Droppable>
          </DragDropContext>
        </Grid>
      </Grid>
    </>
  );
};

export { EditableSection };
