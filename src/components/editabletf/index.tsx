import { Grid, IconButton, Tooltip } from '@material-ui/core';
import { createStyles, fade, makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import { Delete, Done, DragIndicator, Edit, InfoRounded } from '@material-ui/icons';
import { BootstrapInput } from 'components';
import React from 'react';
import { DraggableProvidedDragHandleProps } from 'react-beautiful-dnd';

/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable  @typescript-eslint/no-explicit-any */

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    subtitle: {
      fontSize: 14,
      fontWeight: 'bold',
      marginTop: theme.spacing(2),
    },
    content: {
      //   marginTop: theme.spacing(3),
    },
    selected: {
      color: theme.palette.primary.main,
    },
    section: {
      marginTop: theme.spacing(3),
    },
    margin16: {
      marginTop: theme.spacing(1),
    },
    colorInput: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    padLeft16: {
      paddingLeft: theme.spacing(1),
    },
    actionsContainer: {
      marginTop: theme.spacing(3),
    },

    bordered: {
      //   border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      border: `1px solid ${theme.palette.grey[300]}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
  }),
);

type EditableTFProps = {
  text: string;
  editing?: boolean;
  tfRef?: React.Ref<unknown>;
  tfId: number;
  dragHandleProps?: DraggableProvidedDragHandleProps;
  isDragging?: boolean;
  style?: React.CSSProperties;
  name?: string;
  defaultValue?: string;
  onDelete?: () => void;
  error?: boolean;
  helperText?: string;
};

const EditableTF = ({
  editing = true,
  dragHandleProps,
  isDragging = false,
  style = {},
  name,
  tfRef,
  defaultValue,
  onDelete,
  helperText,
  error,
}: EditableTFProps): JSX.Element => {
  const theme = useTheme();
  const classes = useStyles();
  const [edit, setEdit] = React.useState(editing);
  return (
    <>
      <Grid
        style={{
          display: 'inline-block',
          ...style,
        }}
        container
      >
        <Grid
          container
          alignItems='center'
          justify='space-between'
          className={classes.bordered}
          style={{
            backgroundColor: isDragging ? theme.palette.action.selected : theme.palette.background.paper,
          }}
          item
          md={12}
          sm={12}
          xs={12}
          lg={12}
          xl={12}
        >
          <Grid item container md={10} sm={8} lg={10} xl={10} xs={8} alignItems='center' justify='flex-start' direction='row'>
            <Grid item {...dragHandleProps} md={1} sm={1} lg={1} xs={1} xl={1} container alignItems='center'>
              <DragIndicator color='action' />
            </Grid>
            <Grid item md={10} sm={10} lg={10} xs={10} xl={10}>
              <BootstrapInput
                autoFocus
                // innerRef={input => input && input.focus()}
                readOnly={!edit}
                defaultValue={defaultValue}
                fullWidth
                name={name}
                inputRef={tfRef}
                placeholder='Your text here...'
                onKeyPress={e => {
                  if (e.key === 'Enter') {
                    e.preventDefault();
                    setEdit(!edit);
                  }
                }}
              />
            </Grid>
          </Grid>
          <Grid item md={2} sm={4} lg={2} xl={2} xs={4} container direction='row' justify='flex-end'>
            {error && (
              <Tooltip title={helperText ?? ''}>
                <InfoRounded color='error' />
              </Tooltip>
            )}
            <Grid item>
              <IconButton
                size='small'
                aria-label='edit slot'
                component='span'
                onClick={() => {
                  setEdit(!edit);
                }}
              >
                {edit ? <Done /> : <Edit />}
              </IconButton>
            </Grid>
            <Grid item>
              <IconButton
                size='small'
                aria-label='delete slot'
                component='span'
                onClick={() => {
                  if (onDelete) onDelete();
                }}
              >
                <Delete />
              </IconButton>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export { EditableTF };
