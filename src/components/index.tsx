import { InputBase } from '@material-ui/core';
import { createStyles, Theme, withStyles } from '@material-ui/core/styles';
import { ToggleButton, ToggleButtonGroup } from '@material-ui/lab';

const BootstrapInput = withStyles((theme: Theme) =>
  createStyles({
    root: {
      'label + &': {
        marginTop: theme.spacing(3),
      },
    },
    input: {
      borderRadius: theme.shape.borderRadius,
      position: 'relative',
      //   backgroundColor: theme.palette.common.white,
      //   border: '1px solid #ced4da',
      fontSize: 16,
      padding: '10px 12px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      fontFamily: [theme.typography.fontFamily].join(','),
      '&:focus': {
        // boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.1rem`,
        // borderColor: theme.palette.primary.main,
      },
    },

    disabled: {
      color: theme.palette.text.primary,
    },
  }),
)(InputBase);

const StyledToggleButton = withStyles(theme => ({
  root: {
    fontWeight: 'bold',
    textTransform: 'none',
    paddingLeft: 16,
    paddingRight: 16,
  },
  selected: {},
  label: {
    color: theme.palette.primary.main,
  },
}))(ToggleButton);

const StyledToggleButtonGroup = withStyles(theme => ({
  grouped: {
    border: `1px solid ${theme.palette.action.active}`,
  },
}))(ToggleButtonGroup);

export { BootstrapInput, StyledToggleButton, StyledToggleButtonGroup };
