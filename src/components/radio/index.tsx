import { Radio, RadioProps } from '@material-ui/core';
import { CheckCircle, RadioButtonUnchecked } from '@material-ui/icons/';
import React from 'react';

function StyledRadio(props: RadioProps): JSX.Element {
  return (
    <Radio
      //   className={classes.root}
      //   disableRipple
      color='default'
      checkedIcon={<CheckCircle color='primary' />}
      icon={<RadioButtonUnchecked />}
      {...props}
    />
  );
}

export { StyledRadio };
