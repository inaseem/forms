import { Typography } from '@material-ui/core';
import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    title: {
      fontSize: 18,
      fontWeight: 'bold',
      color: theme.palette.primary.main,
    },
    heading: {
      fontSize: 16,
      fontWeight: 'bold',
      color: theme.palette.text.primary,
    },
    subtitle: {
      fontSize: 14,
    },
    subtitle2: {
      fontSize: 16,
      //   color: theme.palette.primary.light,
    },
    content: {
      marginTop: theme.spacing(3),
    },
    section: {
      marginTop: theme.spacing(2),
    },
  }),
);

type SectionProps = {
  heading: string;
  subtitle: string;
  className?: string;
  title?: boolean;
};

const Section = ({ heading, subtitle, className, title = false }: SectionProps): JSX.Element => {
  const classes = useStyles();
  return (
    <div className={className}>
      <Typography gutterBottom className={title ? classes.title : classes.heading}>
        {heading}
      </Typography>
      <Typography className={title ? classes.subtitle2 : classes.subtitle} color='textSecondary'>
        {subtitle}
      </Typography>
    </div>
  );
};

export { Section };
