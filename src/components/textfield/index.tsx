import { FormControl, FormHelperText, FormHelperTextProps, InputBase, InputBaseProps, InputLabel } from '@material-ui/core';
import { createStyles, fade, Theme, withStyles } from '@material-ui/core/styles';
import React from 'react';

const BootstrapInput = withStyles((theme: Theme) =>
  createStyles({
    root: {
      'label + &': {
        marginTop: theme.spacing(3),
      },
    },
    input: {
      borderRadius: theme.shape.borderRadius,
      position: 'relative',
      backgroundColor: theme.palette.common.white,
      border: '1px solid #ced4da',
      fontSize: 16,
      //   width: 'auto',
      padding: '10px 12px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      // Use the system font instead of the default Roboto font.
      fontFamily: [theme.typography.fontFamily].join(','),
      '&:focus': {
        boxShadow: `${fade(theme.palette.primary.main, 0.25)} 0 0 0 0.1rem`,
        borderColor: theme.palette.primary.main,
      },
    },
  }),
)(InputBase);

type Extras = {
  label: string;
  size: 'normal' | 'small' | 'medium';
  helperText: string;
};

const TextFieldNew = (props: InputBaseProps & Partial<Extras> & FormHelperTextProps): JSX.Element => {
  const { helperText, ...rest } = props;
  //   return <TextField {...props} variant='outlined' />;
  return (
    <FormControl fullWidth={props.fullWidth} style={{ marginTop: 8, marginBottom: 8 }}>
      <InputLabel shrink htmlFor={props.id}>
        {props.label}
      </InputLabel>
      <BootstrapInput {...rest} />
      {helperText && (
        <FormHelperText id={`${props.id}-helper-text`} error={props.error}>
          {helperText}
        </FormHelperText>
      )}
    </FormControl>
  );
};
export { TextFieldNew };
