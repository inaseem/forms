import { yupResolver } from '@hookform/resolvers/yup';
import { Button, FormHelperText, Grid } from '@material-ui/core';
import { createStyles, fade, makeStyles, Theme } from '@material-ui/core/styles';
import { Section } from 'components/section';
import { TextFieldNew } from 'components/textfield';
import { EditorState } from 'draft-js';
import { stateToHTML } from 'draft-js-export-html';
import { stateFromHTML } from 'draft-js-import-html';
import { useAppState, useDispatch } from 'providers';
import React from 'react';
import { Editor } from 'react-draft-wysiwyg';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    subtitle: {
      fontSize: 14,
      fontWeight: 'bold',
      marginTop: theme.spacing(2),
    },
    content: {
      //   marginTop: theme.spacing(3),
    },
    selected: {
      color: theme.palette.primary.main,
    },
    section: {
      marginTop: theme.spacing(3),
    },
    margin16: {
      marginTop: theme.spacing(1),
    },
    colorInput: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    padLeft16: {
      paddingLeft: theme.spacing(1),
    },
    actionsContainer: {
      marginTop: theme.spacing(3),
    },
    wrapperClass: {
      //   minHeight: 120,
      //   borderRadius: theme.shape.borderRadius,
    },
    editorClass: {
      border: `0.5px solid #ced4da`,
      minHeight: 120,
      paddingLeft: theme.spacing(2),
      paddingRight: theme.spacing(2),
      borderRadius: theme.shape.borderRadius,
    },
    toolbarClass: {
      borderRadius: theme.shape.borderRadius,
      //   border: 'none',
    },
  }),
);

const stepInitialValue = {
  to: '',
  subject: '',
  body: '',
};

type StepType = typeof stepInitialValue;

const stepValidationSchema = yup.object().shape({
  to: yup.string().required('This is Required'),
  subject: yup.string().required('This is Required'),
  body: yup.string().required('This is required'),
});

const Mail = (): JSX.Element => {
  const classes = useStyles();
  const [editorState, setEditorState] = React.useState(() => EditorState.createEmpty());
  const dispatch = useDispatch();
  const { form2 } = useAppState();
  const defaultValues = { ...stepInitialValue, ...form2 };

  const { handleSubmit, errors, register, reset, control } = useForm<StepType>({
    resolver: yupResolver(stepValidationSchema),
    defaultValues: defaultValues,
  });

  return (
    <form
      className={classes.content}
      noValidate
      onSubmit={handleSubmit(data => {
        console.log(data);
        dispatch({ type: 'FORM2', payload: data });
        dispatch({ type: 'NEXT' });
      })}
    >
      <Section title heading='Send en email' subtitle='Let visitors get an email from you.' />
      <Grid container className={classes.section}>
        <Grid item sm={12} xs={12} md={7}>
          <TextFieldNew
            fullWidth
            label='To'
            id='to'
            inputRef={register}
            defaultValue={defaultValues.to}
            helperText={errors.to ? errors.to.message : ''}
            error={errors.to ? true : false}
            name='to'
            placeholder='to@mail.com'
          />
        </Grid>
      </Grid>

      <Grid container spacing={2} className={classes.margin16}>
        <Grid item sm={12} xs={12} md={7}>
          <TextFieldNew
            fullWidth
            label='Subject'
            id='subject'
            inputRef={register}
            defaultValue={defaultValues.subject}
            helperText={errors.subject ? errors.subject.message : ''}
            error={errors.subject ? true : false}
            name='subject'
            placeholder='Thanks and welcome.'
          />
        </Grid>
        <Grid item sm={12} xs={12} md={12}>
          <Controller
            name='body'
            control={control}
            defaultValue={defaultValues.body}
            render={({ value, onChange, onBlur }) => (
              <Editor
                defaultEditorState={EditorState.createWithContent(stateFromHTML(value))}
                wrapperClassName={classes.wrapperClass}
                editorClassName={classes.editorClass}
                toolbarClassName={classes.toolbarClass}
                onBlur={onBlur}
                onEditorStateChange={prop => {
                  const html = stateToHTML(editorState.getCurrentContent());
                  onChange(html);
                  setEditorState(prop);
                }}
                placeholder='Email body here...'
                mention={{
                  separator: ' ',
                  trigger: '@',
                  suggestions: [
                    { text: 'APPLE', value: 'apple', url: 'apple' },
                    { text: 'BANANA', value: 'banana', url: 'banana' },
                    { text: 'CHERRY', value: 'cherry', url: 'cherry' },
                    { text: 'DURIAN', value: 'durian', url: 'durian' },
                    { text: 'EGGFRUIT', value: 'eggfruit', url: 'eggfruit' },
                    { text: 'FIG', value: 'fig', url: 'fig' },
                    { text: 'GRAPEFRUIT', value: 'grapefruit', url: 'grapefruit' },
                    { text: 'HONEYDEW', value: 'honeydew', url: 'honeydew' },
                  ],
                }}
              />
            )}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {errors.body && <FormHelperText error={errors.body ? true : false}>{errors.body.message}</FormHelperText>}
        </Grid>
      </Grid>

      <Grid container className={classes.actionsContainer} spacing={1}>
        <Grid item>
          <Button type='submit' variant='contained' color='primary' disableElevation style={{ color: 'white' }}>
            Save
          </Button>
        </Grid>
        <Grid item>
          <Button variant='outlined' color='primary' disableElevation onClick={() => reset()}>
            Cancel
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Mail;
