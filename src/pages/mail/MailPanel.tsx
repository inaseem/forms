import { Drawer, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { createStyles, fade, makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import React, { useState } from 'react';
import Mail from './Mail';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { display: 'flex' },
    // sidebar: { paddingTop: theme.spacing(3) },
    content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(3),
      paddingLeft: theme.spacing(6),
    },
    title: {
      fontSize: 18,
      fontWeight: 'bold',
      color: theme.palette.text.primary,
    },
    subtitle: {
      fontSize: 15,
      marginTop: theme.spacing(2),
    },

    selected: {
      color: theme.palette.primary.main,
    },
    section: {
      marginTop: theme.spacing(3),
    },
    margin16: {
      marginTop: theme.spacing(1),
    },
    colorInput: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    padLeft16: {
      paddingLeft: theme.spacing(1),
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
  }),
);

const items = [
  {
    name: 'Send an email',
    id: 0,
    component: <Mail />,
  },
];

const MailPanel = (): JSX.Element => {
  const classes = useStyles();
  const theme = useTheme();
  const [current] = useState(0);
  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant='permanent'
        classes={{
          paper: classes.drawerPaper,
        }}
        PaperProps={{ style: { borderRight: 'none' } }}
        elevation={0}
        anchor='left'
      >
        <List component='nav' aria-label='main mailbox folders'>
          {items.map(item => (
            <ListItem
              style={{ borderTopRightRadius: theme.shape.borderRadius, borderBottomRightRadius: theme.shape.borderRadius }}
              button
              key={item.id}
              selected={true}
            >
              <ListItemIcon>
                <CheckCircleIcon />
              </ListItemIcon>
              <ListItemText primary={items[item.id].name} />
            </ListItem>
          ))}
        </List>
      </Drawer>
      <main className={classes.content}>{items[current].component}</main>
    </div>
  );
};

export default MailPanel;
