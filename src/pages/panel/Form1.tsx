import { yupResolver } from '@hookform/resolvers/yup';
import { Button, FormHelperText, Grid, InputBase, InputLabel, Typography } from '@material-ui/core';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { StyledToggleButton, StyledToggleButtonGroup } from 'components';
import { Section } from 'components/section';
import { useDispatch, useAppState } from 'providers';
import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    subtitle: {
      fontSize: 14,
      fontWeight: 'bold',
      marginTop: theme.spacing(2),
    },
    content: {
      //   marginTop: theme.spacing(3),
    },
    selected: {
      color: theme.palette.primary.main,
    },
    section: {
      marginTop: theme.spacing(3),
    },
    margin16: {
      marginTop: theme.spacing(1),
    },
    actionsContainer: {
      marginTop: theme.spacing(3),
    },
    colorInput: {
      border: `1px solid ${theme.palette.grey[300]}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    padLeft16: {
      paddingLeft: theme.spacing(1),
    },
  }),
);

const stepInitialValue = {
  activationStatusF1: '',
  positionF1: '',
  primaryF1: '#000000',
  linkF1: '#000000',
};

type StepType = typeof stepInitialValue;

const stepValidationSchema = yup.object().shape({
  activationStatusF1: yup.string().required('Activation status is Required'),
  positionF1: yup.string().required('Position is Required'),
  primaryF1: yup.string().required('Primary color is required'),
  linkF1: yup.string().required('Link color is required'),
});

const Form1 = (): JSX.Element => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { form1 } = useAppState();
  const defaultValues = { ...stepInitialValue, ...form1 };

  const { handleSubmit, errors, control, reset } = useForm<StepType>({
    resolver: yupResolver(stepValidationSchema),
    defaultValues: defaultValues,
  });
  //   console.log(form1);

  return (
    <form
      className={classes.content}
      noValidate
      onSubmit={handleSubmit(data => {
        console.log(data);
        dispatch({ type: 'FORM1', payload: data });
        dispatch({ type: 'NEXT' });
      })}
    >
      <Section title heading='Style your panel' subtitle='Add your logo, choose colors and wallpapers, and turn sounds off or on' />
      <Section
        className={classes.section}
        heading='Activation Status'
        subtitle='Control whether or not the widget appears on your website'
      />
      <Grid container>
        <Grid item>
          <Controller
            control={control}
            name='activationStatusF1'
            defaultValue={defaultValues.activationStatusF1}
            render={({ value, ref, onBlur, onChange }) => (
              <StyledToggleButtonGroup
                className={classes.margin16}
                value={value}
                exclusive
                ref={ref}
                onBlur={onBlur}
                onChange={(event: React.MouseEvent<HTMLElement>, newVal: string | null) => onChange(newVal)}
                size='small'
              >
                <StyledToggleButton value='on'>On</StyledToggleButton>
                <StyledToggleButton value='off'>Off</StyledToggleButton>
              </StyledToggleButtonGroup>
            )}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {errors.activationStatusF1 && (
            <FormHelperText error={errors.activationStatusF1 ? true : false}>{errors.activationStatusF1.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Section className={classes.section} heading='Position' subtitle='Choose which side of the page the widget should appear' />
      <Grid container>
        <Grid item>
          <Controller
            control={control}
            name='positionF1'
            defaultValue={defaultValues.positionF1}
            render={({ value, ref, onBlur, onChange }) => (
              <StyledToggleButtonGroup
                className={classes.margin16}
                value={value}
                exclusive
                ref={ref}
                onBlur={onBlur}
                onChange={(event: React.MouseEvent<HTMLElement>, newVal: string | null) => onChange(newVal)}
                size='small'
              >
                <StyledToggleButton value='left'>Left</StyledToggleButton>
                <StyledToggleButton value='right'>Right</StyledToggleButton>
              </StyledToggleButtonGroup>
            )}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {errors.positionF1 && <FormHelperText error={errors.positionF1 ? true : false}>{errors.positionF1.message}</FormHelperText>}
        </Grid>
      </Grid>
      <Section className={classes.section} heading='Color' subtitle='Customize the color of your widget by choosing a color' />
      <Typography className={classes.subtitle} color='textSecondary'>
        Primary Color
      </Typography>
      <Grid style={{ display: 'inline-block' }} container>
        <Controller
          control={control}
          name='primaryF1'
          defaultValue={defaultValues.primaryF1}
          render={({ value, ref, onBlur, onChange }) => (
            <Grid container alignItems='center' justify='space-between' className={classes.colorInput} item md={7}>
              <Grid md={11} item>
                <InputLabel shrink={false} htmlFor='primaryF1'>
                  {value}
                </InputLabel>
              </Grid>
              <Grid item className={classes.padLeft16}>
                <InputBase
                  onBlur={onBlur}
                  //   onChange={e => onChange(e.target.value)}
                  onChange={onChange}
                  inputRef={ref}
                  id='primaryF1'
                  inputProps={{ style: { width: 24, height: 24 } }}
                  type='color'
                  defaultValue={value}
                />
              </Grid>
            </Grid>
          )}
        />
      </Grid>
      <FormHelperText error={errors.primaryF1 ? true : false}>
        {errors.primaryF1 ? errors.primaryF1.message : 'This is the primary color for your panel'}
      </FormHelperText>
      {/* Link Color */}
      <Typography className={classes.subtitle} color='textSecondary'>
        Link Color
      </Typography>
      <Grid style={{ display: 'inline-block' }} container>
        <Controller
          control={control}
          name='linkF1'
          defaultValue={defaultValues.linkF1}
          render={({ value, ref, onBlur, onChange }) => (
            <Grid container alignItems='center' justify='space-between' className={classes.colorInput} item md={7}>
              <Grid item md={11}>
                <InputLabel shrink={false} htmlFor='linkF1'>
                  {value}
                </InputLabel>
              </Grid>
              <Grid item className={classes.padLeft16}>
                <InputBase
                  onBlur={onBlur}
                  //   onChange={e => onChange(e.target.value)}
                  onChange={onChange}
                  inputRef={ref}
                  id='linkF1'
                  inputProps={{ style: { width: 24, height: 24 } }}
                  type='color'
                  defaultValue={value}
                />
              </Grid>
            </Grid>
          )}
        />
      </Grid>
      <FormHelperText>If we want to add more colors they will look like this.</FormHelperText>
      <Grid container className={classes.actionsContainer} spacing={1}>
        <Grid item>
          <Button type='submit' variant='contained' color='primary' disableElevation style={{ color: 'white' }}>
            Save
          </Button>
        </Grid>
        <Grid item>
          <Button variant='outlined' color='primary' disableElevation onClick={() => reset()}>
            Cancel
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Form1;
