import { yupResolver } from '@hookform/resolvers/yup';
import { Button, FormHelperText, Grid } from '@material-ui/core';
import { createStyles, fade, makeStyles, Theme } from '@material-ui/core/styles';
import { StyledToggleButton, StyledToggleButtonGroup } from 'components';
import { Section } from 'components/section';
import { TextFieldNew } from 'components/textfield';
import { useDispatch, useAppState } from 'providers';
import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    subtitle: {
      fontSize: 14,
      fontWeight: 'bold',
      marginTop: theme.spacing(2),
    },
    content: {
      //   marginTop: theme.spacing(3),
    },
    selected: {
      color: theme.palette.primary.main,
    },
    section: {
      marginTop: theme.spacing(3),
    },
    margin16: {
      marginTop: theme.spacing(1),
    },
    colorInput: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    padLeft16: {
      paddingLeft: theme.spacing(1),
    },
    actionsContainer: {
      marginTop: theme.spacing(3),
    },
  }),
);

const stepInitialValue = {
  visibilityF2: '',
  titleF2: '',
  textF2: '',
  additionalF2: '',
};

type StepType = typeof stepInitialValue;

const stepValidationSchema = yup.object().shape({
  visibilityF2: yup.string().required('This is Required'),
  titleF2: yup.string().required('This is Required'),
  textF2: yup.string().required('This is required'),
  additionalF2: yup.string().required('This is required'),
});

const Form2 = (): JSX.Element => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { form2 } = useAppState();
  const defaultValues = { ...stepInitialValue, ...form2 };

  const { handleSubmit, errors, control, register, reset } = useForm<StepType>({
    resolver: yupResolver(stepValidationSchema),
    defaultValues: defaultValues,
  });

  return (
    <form
      className={classes.content}
      noValidate
      onSubmit={handleSubmit(data => {
        console.log(data);
        dispatch({ type: 'FORM2', payload: data });
        dispatch({ type: 'NEXT' });
      })}
    >
      <Section title heading='Request a call' subtitle='Let visitors request a call from you.' />
      <Section className={classes.section} heading='Visiblity' subtitle='When should this be shown?' />
      <Grid container>
        <Grid item>
          <Controller
            control={control}
            name='visibilityF2'
            defaultValue={defaultValues.visibilityF2}
            render={({ value, ref, onBlur, onChange }) => (
              <StyledToggleButtonGroup
                className={classes.margin16}
                value={value}
                exclusive
                ref={ref}
                onBlur={onBlur}
                onChange={(e, newVal) => onChange(newVal)}
                size='small'
              >
                <StyledToggleButton value='always'>Always</StyledToggleButton>
                <StyledToggleButton value='workingHrs'>Working hours only</StyledToggleButton>
                <StyledToggleButton value='afterHrs'>After hours only</StyledToggleButton>
              </StyledToggleButtonGroup>
            )}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {errors.visibilityF2 && <FormHelperText error={errors.visibilityF2 ? true : false}>{errors.visibilityF2.message}</FormHelperText>}
        </Grid>
      </Grid>

      <Section
        className={classes.section}
        heading='An introduction text'
        subtitle='This is the greeting text that sits at the top of the panel'
      />

      <Grid container spacing={2} className={classes.margin16}>
        <Grid item sm={12} xs={12} md={7}>
          <TextFieldNew
            fullWidth
            label='Title'
            id='titleF2'
            inputRef={register}
            defaultValue={defaultValues.titleF2}
            helperText={errors.titleF2 ? errors.titleF2.message : ''}
            error={errors.titleF2 ? true : false}
            name='titleF2'
            placeholder='Title example'
          />
        </Grid>
        <Grid item sm={12} xs={12} md={7}>
          <TextFieldNew
            fullWidth
            multiline
            inputRef={register}
            defaultValue={defaultValues.textF2}
            rows={5}
            id='textF2'
            helperText={errors.textF2 ? errors.textF2.message : ''}
            error={errors.textF2 ? true : false}
            label='Text'
            name='textF2'
            type='text'
            size='small'
            placeholder='I am an example of text that sits below the title'
          />
        </Grid>
      </Grid>

      <Section
        className={classes.section}
        heading='Collect additional information once form is submitted'
        subtitle='This is the intro text that tells the visitor what they can register for.'
      />
      <Grid container>
        <Grid item>
          <Controller
            control={control}
            name='additionalF2'
            defaultValue={defaultValues.additionalF2}
            render={({ value, ref, onBlur, onChange }) => (
              <StyledToggleButtonGroup
                className={classes.margin16}
                value={value}
                exclusive
                ref={ref}
                onBlur={onBlur}
                onChange={(e, newVal) => onChange(newVal)}
                size='small'
              >
                <StyledToggleButton value='on'>On</StyledToggleButton>
                <StyledToggleButton value='off'>Off</StyledToggleButton>
              </StyledToggleButtonGroup>
            )}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {errors.additionalF2 && <FormHelperText error={errors.additionalF2 ? true : false}>{errors.additionalF2.message}</FormHelperText>}
        </Grid>
      </Grid>

      <Grid container className={classes.actionsContainer} spacing={1}>
        <Grid item>
          <Button type='submit' variant='contained' color='primary' disableElevation style={{ color: 'white' }}>
            Save
          </Button>
        </Grid>
        <Grid item>
          <Button variant='outlined' color='primary' disableElevation onClick={() => reset()}>
            Cancel
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Form2;
