import { yupResolver } from '@hookform/resolvers/yup';
import { Button, Grid, IconButton, TextField, Typography } from '@material-ui/core';
import { createStyles, fade, makeStyles, Theme } from '@material-ui/core/styles';
import { Delete } from '@material-ui/icons';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { Section } from 'components/section';
import { useAppState, useDispatch } from 'providers';
import React from 'react';
import { useFieldArray, useForm } from 'react-hook-form';
import * as yup from 'yup';

/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable  @typescript-eslint/no-explicit-any */

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    subtitle: {
      fontSize: 14,
      fontWeight: 'bold',
      marginTop: theme.spacing(2),
    },
    content: {
      //   marginTop: theme.spacing(3),
    },
    selected: {
      color: theme.palette.primary.main,
    },
    section: {
      marginTop: theme.spacing(3),
    },
    margin16: {
      marginTop: theme.spacing(1),
    },
    colorInput: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    padLeft16: {
      paddingLeft: theme.spacing(1),
    },
    actionsContainer: {
      marginTop: theme.spacing(3),
    },
  }),
);
const divisions = 6;
const minutes = Array.from(new Array(divisions)).map((_, i) => `${(i * 60) / divisions}`.padStart(2, '0'));

const timeSlots = Array.from(new Array(24 * divisions)).map(
  (_, index) => `${Math.floor(index / divisions)}`.padStart(2, '0') + `:${minutes[index % divisions]}`,
);

const daySlots = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const initialValues = {
  hoursF3: [],
};

type StepType = typeof initialValues;

const stepValidationSchema = yup.object().shape({
  hoursF3: yup.array().of(
    yup.object().shape({
      day: yup.string().oneOf(daySlots, 'Day must be one of the drop values'),
      start: yup.string().oneOf(timeSlots, 'Time must be one of the drop down values'),
      end: yup.string().oneOf(timeSlots, 'Time must be one of the drop down values'),
    }),
  ),
});

const Form3 = (): JSX.Element => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { form3 } = useAppState();
  const defaultValues = { ...initialValues, ...form3 };
  const { control, register, errors, handleSubmit, reset } = useForm<StepType>({
    resolver: yupResolver(stepValidationSchema),
    defaultValues: defaultValues,
    reValidateMode: 'onBlur',
  });
  const { fields, append, remove } = useFieldArray({
    control, // control props comes from useForm (optional: if you are using FormContext)
    name: 'hoursF3', // unique name for your Field Array
  });

  const hasError = (errors: any, index: number, prop: any) => {
    try {
      if (!errors) return false;
      if (!errors.hoursF3) return false;
      const arr = errors.hoursF3[index];
      if (arr[prop]) return true;
      return false;
    } catch (e) {
      return false;
    }
  };

  return (
    <form
      noValidate
      className={classes.content}
      onSubmit={handleSubmit(data => {
        console.log(data);
        dispatch({ type: 'FORM3', payload: data });
        dispatch({ type: 'NEXT' });
      })}
    >
      <Section
        title
        heading='Set hours of operation'
        subtitle='Set up your engagement panel based on your hours of operation and availability.'
      />
      <Grid container>
        <Grid item md={12} sm={12} xs={12} lg={12} xl={12}>
          {fields.map((field, index) => {
            return (
              <Grid container alignItems='flex-start' spacing={2} className={classes.section} key={field.id}>
                <Grid item sm={3} md={3} xs={12} lg={3} xl={3}>
                  <Autocomplete
                    size='small'
                    id='day'
                    fullWidth
                    defaultValue={field.day}
                    options={daySlots}
                    getOptionDisabled={option => option === daySlots[0]}
                    renderInput={params => (
                      <TextField
                        error={hasError(errors, index, 'day')}
                        helperText={hasError(errors, index, 'day') ? 'Invalid entry' : ''}
                        inputRef={register()}
                        name={`hoursF3[${index}].day`}
                        {...params}
                        label='Day'
                        variant='outlined'
                      />
                    )}
                  />
                </Grid>
                <Grid item sm={3} md={3} xs={12} lg={3} xl={3}>
                  <Autocomplete
                    size='small'
                    id='day'
                    fullWidth
                    defaultValue={field.start}
                    options={timeSlots}
                    renderInput={params => (
                      <TextField
                        error={hasError(errors, index, 'start')}
                        inputRef={register()}
                        name={`hoursF3[${index}].start`}
                        helperText={hasError(errors, index, 'start') ? 'Invalid entry' : ''}
                        {...params}
                        label='Start'
                        variant='outlined'
                      />
                    )}
                  />
                </Grid>
                <Grid item>
                  <Typography style={{ padding: '6px' }}>to</Typography>
                </Grid>
                <Grid item sm={3} md={3} xs={12} lg={3} xl={3}>
                  <Autocomplete
                    size='small'
                    id='day'
                    defaultValue={field.end}
                    options={timeSlots}
                    renderInput={params => (
                      <TextField
                        error={hasError(errors, index, 'end')}
                        helperText={hasError(errors, index, 'end') ? 'Invalid entry' : ''}
                        inputRef={register()}
                        name={`hoursF3[${index}].end`}
                        {...params}
                        label='End'
                        variant='outlined'
                      />
                    )}
                  />
                </Grid>
                <Grid item sm={2} container>
                  <Grid item>
                    <IconButton
                      aria-label='delete slot'
                      component='span'
                      onClick={() => {
                        remove(index);
                      }}
                    >
                      <Delete />
                    </IconButton>
                  </Grid>
                </Grid>
              </Grid>
            );
          })}
        </Grid>
        {/* <Grid item md={12} sm={12} xs={12}>
          {errors.hoursF3 && <FormHelperText error={errors.hoursF3 ? true : false}>{JSON.stringify(errors, null, 2)}</FormHelperText>}
        </Grid> */}
      </Grid>

      <Button
        variant='text'
        color='primary'
        disableElevation
        style={{ textTransform: 'none', fontWeight: 'bold', marginTop: 24 }}
        onClick={() => {
          append({ day: 'Monday', start: '00:00', end: '00:00' });
          //   append({ day: '', from: '', to: '' });
        }}
      >
        + Add hours
      </Button>

      <Grid container className={classes.actionsContainer} spacing={1}>
        <Grid item>
          <Button type='submit' variant='contained' color='primary' disableElevation style={{ color: 'white' }}>
            Save
          </Button>
        </Grid>
        <Grid item>
          <Button variant='outlined' color='primary' disableElevation onClick={() => reset()}>
            Cancel
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Form3;
