import { Button, FormHelperText, Grid } from '@material-ui/core';
import { createStyles, fade, makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import { StyledToggleButton, StyledToggleButtonGroup } from 'components';
import { EditableTF } from 'components/editabletf';
import { Section } from 'components/section';
import { useAppState, useDispatch } from 'providers';
import React from 'react';
import { DragDropContext, Draggable, Droppable, DropResult } from 'react-beautiful-dnd';
import { Controller, useFieldArray, useForm } from 'react-hook-form';
import * as yup from 'yup';
import { yupResolver } from '@hookform/resolvers/yup';
import { hasError, getError } from 'utils';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    subtitle: {
      fontSize: 14,
      fontWeight: 'bold',
      marginTop: theme.spacing(2),
    },
    content: {
      //   marginTop: theme.spacing(3),
    },
    selected: {
      color: theme.palette.primary.main,
    },
    section: {
      marginTop: theme.spacing(3),
    },
    margin16: {
      marginTop: theme.spacing(1),
    },
    colorInput: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    padLeft16: {
      paddingLeft: theme.spacing(1),
    },
    actionsContainer: {
      marginTop: theme.spacing(3),
    },

    bordered: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
  }),
);

const initialValues = {
  demoTypesF4: [],
  visibilityF4: '',
};

type StepType = typeof initialValues;

const stepValidationSchema = yup.object().shape({
  demoTypesF4: yup
    .array()
    .of(
      yup.object().shape({
        value: yup.string().required('This is required'),
      }),
    )
    .required('This is required'),
  visibilityF4: yup.string().required('This is required'),
});

const Form4 = (): JSX.Element => {
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const { form4 } = useAppState();
  const defaultValues = { ...initialValues, ...form4 };
  const { control, register, errors, handleSubmit, reset } = useForm<StepType>({
    resolver: yupResolver(stepValidationSchema),
    defaultValues: defaultValues,
  });
  const { fields, append, remove, move } = useFieldArray({
    control, // control props comes from useForm (optional: if you are using FormContext)
    name: 'demoTypesF4', // unique name for your Field Array
  });

  const onDragged = (result: DropResult) => {
    const { destination, source } = result;
    if (!destination) return;
    if (destination.droppableId == source.droppableId && destination.index == source.index) return;
    move(source.index, destination.index);
  };

  return (
    <form
      className={classes.content}
      noValidate
      onSubmit={handleSubmit(data => {
        console.log(data);
        dispatch({ type: 'FORM4', payload: data });
        dispatch({ type: 'NEXT' });
      })}
    >
      <Section title heading='Schedule a demo' subtitle='Allow visitors to schedule a meeting with you.' />
      <Section className={classes.section} heading='Visiblity' subtitle='When should this be shown?' />
      <Grid container>
        <Grid item>
          <Controller
            control={control}
            name='visibilityF4'
            defaultValue={defaultValues.visibilityF4}
            render={({ value, ref, onBlur, onChange }) => (
              <StyledToggleButtonGroup
                className={classes.margin16}
                value={value}
                ref={ref}
                exclusive
                size='small'
                defaultValue={value}
                onBlur={onBlur}
                onChange={(e, val) => onChange(val)}
              >
                <StyledToggleButton value='always'>Always</StyledToggleButton>
                <StyledToggleButton value='workingHrs'>Working hours only</StyledToggleButton>
                <StyledToggleButton value='afterHrs'>After hours only</StyledToggleButton>
              </StyledToggleButtonGroup>
            )}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {errors.visibilityF4 && <FormHelperText error={errors.visibilityF4 ? true : false}>{errors.visibilityF4.message}</FormHelperText>}
        </Grid>
      </Grid>
      <Section className={classes.section} heading='Demo types' subtitle='Text explaining what this means' />

      <DragDropContext onDragEnd={onDragged}>
        <Droppable droppableId='droppable-1'>
          {provided => (
            <Grid container {...provided.droppableProps} ref={provided.innerRef}>
              {fields.map((field, index) => (
                <Draggable draggableId={`${field.id}`} index={index} key={field.id}>
                  {(provided, snapshot) => (
                    <Grid item md={12} sm={12} lg={7} xl={7} xs={12} {...provided.draggableProps} ref={provided.innerRef}>
                      <EditableTF
                        style={{ marginTop: theme.spacing(1) }}
                        isDragging={snapshot.isDragging}
                        dragHandleProps={provided.dragHandleProps}
                        text=''
                        key={field.id}
                        tfId={index}
                        tfRef={register()}
                        defaultValue={field.value}
                        name={`demoTypesF4[${index}].value`}
                        onDelete={() => {
                          remove(index);
                        }}
                      />
                    </Grid>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </Grid>
          )}
        </Droppable>
      </DragDropContext>
      <Grid container>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {hasError(errors, 'demoTypesF4') && (
            <FormHelperText error={errors.demoTypesF4 ? true : false}>{getError(errors, 'demoTypesF4')}</FormHelperText>
          )}
        </Grid>
      </Grid>
      <Button
        variant='text'
        color='primary'
        disableElevation
        style={{ textTransform: 'none', fontWeight: 'bold', marginTop: 24 }}
        onClick={() => {
          append({ value: '' });
        }}
      >
        + Add a new demo type
      </Button>

      <Grid container className={classes.actionsContainer} spacing={1}>
        <Grid item>
          <Button type='submit' variant='contained' color='primary' disableElevation style={{ color: 'white' }}>
            Save
          </Button>
        </Grid>
        <Grid item>
          <Button variant='outlined' color='primary' disableElevation onClick={() => reset()}>
            Cancel
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Form4;
