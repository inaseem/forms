import { yupResolver } from '@hookform/resolvers/yup';
import { Button, FormHelperText, Grid } from '@material-ui/core';
import { createStyles, fade, makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import { StyledToggleButton, StyledToggleButtonGroup } from 'components';
import { EditableSection } from 'components/editablesection';
import { Section } from 'components/section';
import { useAppState, useDispatch } from 'providers';
import React from 'react';
import { DragDropContext, Draggable, Droppable, DropResult } from 'react-beautiful-dnd';
import { Controller, useFieldArray, useForm } from 'react-hook-form';
import { getError, hasError } from 'utils';
import * as yup from 'yup';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    subtitle: {
      fontSize: 14,
      fontWeight: 'bold',
      marginTop: theme.spacing(2),
    },
    content: {
      //   marginTop: theme.spacing(3),
    },
    selected: {
      color: theme.palette.primary.main,
    },
    section: {
      marginTop: theme.spacing(3),
    },
    margin16: {
      marginTop: theme.spacing(1),
    },
    colorInput: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    padLeft16: {
      paddingLeft: theme.spacing(1),
    },
    actionsContainer: {
      marginTop: theme.spacing(3),
    },

    bordered: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
  }),
);

const initialValues = {
  sectionsF5: [],
  visibilityF5: '',
};

type StepType = {
  sectionsF5: {
    items: {
      value: string;
    }[];
    value: string;
  }[];
  visibilityF5: string;
};

const stepValidationSchema = yup.object().shape({
  sectionsF5: yup
    .array()
    .of(
      yup.object().shape({
        items: yup.array().of(
          yup.object().shape({
            value: yup.string().required('This is required'),
          }),
        ),
        value: yup.string().required('This is required'),
      }),
    )
    .required('At least 1 is required'),
  visibilityF5: yup.string().required('This is required'),
});

const Form5 = (): JSX.Element => {
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const { form5 } = useAppState();
  const defaultValues = { ...initialValues, ...form5 };
  const { control, register, errors, handleSubmit, reset } = useForm<StepType>({
    resolver: yupResolver(stepValidationSchema),
    defaultValues: defaultValues,
    mode: 'onSubmit',
  });

  const { fields, append, remove, move } = useFieldArray({
    control, // control props comes from useForm (optional: if you are using FormContext)
    name: 'sectionsF5', // unique name for your Field Array
  });

  const onDragged = (result: DropResult) => {
    const { destination, source } = result;
    if (!destination) return;
    if (destination.droppableId == source.droppableId && destination.index == source.index) return;
    move(source.index, destination.index);
  };

  return (
    <form
      className={classes.content}
      noValidate
      onSubmit={handleSubmit(data => {
        console.log('Values', data);
        dispatch({ type: 'FORM5', payload: data });
        dispatch({ type: 'NEXT' });
      })}
    >
      <Section title heading='Knowledge base' subtitle='Show answers to commonly asked questions.' />
      <Section className={classes.section} heading='Visiblity' subtitle='When should this be shown?' />
      <Grid container>
        <Grid item>
          <Controller
            control={control}
            name='visibilityF5'
            defaultValue={defaultValues.visibilityF5}
            render={({ value, ref, onBlur, onChange }) => (
              <StyledToggleButtonGroup
                className={classes.margin16}
                exclusive
                size='small'
                ref={ref}
                onBlur={onBlur}
                value={value}
                onChange={(e, val) => onChange(val)}
              >
                <StyledToggleButton value='always'>Always</StyledToggleButton>
                <StyledToggleButton value='workingHrs'>Working hours only</StyledToggleButton>
                <StyledToggleButton value='afterHrs'>After hours only</StyledToggleButton>
              </StyledToggleButtonGroup>
            )}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {errors.visibilityF5 && <FormHelperText error={errors.visibilityF5 ? true : false}>{errors.visibilityF5.message}</FormHelperText>}
        </Grid>
      </Grid>
      <Section className={classes.section} heading='Questions and answers' subtitle='Text explaining what these are' />

      <DragDropContext onDragEnd={onDragged}>
        <Droppable droppableId='droppable-2'>
          {provided => (
            <Grid container {...provided.droppableProps} ref={provided.innerRef}>
              {fields.map((field, index) => (
                <Draggable draggableId={`${field.id}`} index={index} key={field.id}>
                  {(provided, snapshot) => (
                    <Grid item md={12} sm={12} xl={7} lg={7} xs={12} {...provided.draggableProps} ref={provided.innerRef}>
                      <EditableSection
                        style={{ marginTop: theme.spacing(2) }}
                        isDragging={snapshot.isDragging}
                        dragHandleProps={provided.dragHandleProps}
                        text=''
                        key={field.id}
                        sfId={index}
                        tfRef={register()}
                        nestIndex={index}
                        {...{ control, register, errors }}
                        defaultValue={field.value}
                        // defaultValueItems={field.items}
                        name={`sectionsF5[${index}].value`}
                        onDelete={() => {
                          remove(index);
                        }}
                        onItemDelete={() => {
                          //   clearErrors(`sectionsF5[${index}].items`);
                        }}
                      />
                    </Grid>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </Grid>
          )}
        </Droppable>
      </DragDropContext>
      <Grid container>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {hasError(errors, 'sectionsF5') && (
            <FormHelperText error={errors.sectionsF5 ? true : false}>{getError(errors, 'sectionsF5')}</FormHelperText>
          )}
        </Grid>
      </Grid>
      <Button
        variant='text'
        color='primary'
        disableElevation
        style={{ textTransform: 'none', fontWeight: 'bold', marginTop: 24 }}
        onClick={() => {
          append({ items: [], value: '' });
          //   clearErrors('sectionsF5');
        }}
      >
        + Add a section
      </Button>

      <Grid container className={classes.actionsContainer} spacing={1}>
        <Grid item>
          <Button type='submit' variant='contained' color='primary' disableElevation style={{ color: 'white' }}>
            Save
          </Button>
        </Grid>
        <Grid item>
          <Button variant='outlined' color='primary' disableElevation onClick={() => reset()}>
            Cancel
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Form5;
