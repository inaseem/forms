import { yupResolver } from '@hookform/resolvers/yup';
import { Button, FormHelperText, Grid } from '@material-ui/core';
import { createStyles, fade, makeStyles, Theme } from '@material-ui/core/styles';
import { StyledToggleButton, StyledToggleButtonGroup } from 'components';
import { Section } from 'components/section';
import { TextFieldNew } from 'components/textfield';
import { useAppState, useDispatch } from 'providers';
import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    subtitle: {
      fontSize: 14,
      fontWeight: 'bold',
      marginTop: theme.spacing(2),
    },
    content: {
      //   marginTop: theme.spacing(3),
    },
    selected: {
      color: theme.palette.primary.main,
    },
    section: {
      marginTop: theme.spacing(3),
    },
    margin16: {
      marginTop: theme.spacing(1),
    },
    colorInput: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    padLeft16: {
      paddingLeft: theme.spacing(1),
    },
    actionsContainer: {
      marginTop: theme.spacing(3),
    },
  }),
);

const stepInitialValue = {
  visibilityF6: '',
  titleF6: '',
  urlF6: '',
};

type StepType = typeof stepInitialValue;

const stepValidationSchema = yup.object().shape({
  visibilityF6: yup.string().required('This is required'),
  titleF6: yup.string().required('This is required'),
  urlF6: yup.string().url('Invalid url').required('This is required'),
});

const Form6 = (): JSX.Element => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { form6 } = useAppState();
  const defaultValues = { ...stepInitialValue, ...form6 };

  const { handleSubmit, errors, control, register, reset } = useForm<StepType>({
    resolver: yupResolver(stepValidationSchema),
    defaultValues: defaultValues,
  });

  return (
    <form
      className={classes.content}
      noValidate
      onSubmit={handleSubmit(data => {
        console.log(data);
        dispatch({ type: 'FORM6', payload: data });
        dispatch({ type: 'NEXT' });
      })}
    >
      <Section title heading='Link' subtitle='Add a custom link to your engagament panel.' />
      <Section className={classes.section} heading='Visiblity' subtitle='When should this be shown?' />
      <Grid container>
        <Grid item>
          <Controller
            control={control}
            name='visibilityF6'
            defaultValue={defaultValues.visibilityF6}
            render={({ value, ref, onBlur, onChange }) => (
              <StyledToggleButtonGroup
                ref={ref}
                onBlur={onBlur}
                className={classes.margin16}
                value={value}
                exclusive
                defaultValue={defaultValues.visibilityF6}
                onChange={(e, val) => onChange(val)}
                size='small'
              >
                <StyledToggleButton value='always'>Always</StyledToggleButton>
                <StyledToggleButton value='workingHrs'>Working hours only</StyledToggleButton>
                <StyledToggleButton value='afterHrs'>After hours only</StyledToggleButton>
              </StyledToggleButtonGroup>
            )}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {errors.visibilityF6 && <FormHelperText error={errors.visibilityF6 ? true : false}>{errors.visibilityF6.message}</FormHelperText>}
        </Grid>
      </Grid>
      <Section className={classes.section} heading='Link settings' subtitle='You can specify the website address to launch below.' />

      <Grid container spacing={2} className={classes.margin16}>
        <Grid item sm={12} xs={12} md={7} lg={7} xl={7}>
          <TextFieldNew
            inputRef={register}
            helperText={errors.titleF6 ? errors.titleF6.message : ''}
            error={errors.titleF6 ? true : false}
            fullWidth
            defaultValue={defaultValues.titleF6}
            label='Button title'
            name='titleF6'
            placeholder='Title example'
          />
        </Grid>
        <Grid item sm={12} xs={12} md={7} lg={7} xl={7}>
          <TextFieldNew
            fullWidth
            inputRef={register}
            defaultValue={defaultValues.urlF6}
            id='urlF6'
            helperText={errors.urlF6 ? errors.urlF6.message : ''}
            error={errors.urlF6 ? true : false}
            label='URL'
            name='urlF6'
            type='text'
            size='small'
            placeholder='http://example.com'
          />
        </Grid>
      </Grid>

      <Grid container className={classes.actionsContainer} spacing={1}>
        <Grid item>
          <Button type='submit' variant='contained' color='primary' disableElevation style={{ color: 'white' }}>
            Save
          </Button>
        </Grid>
        <Grid item>
          <Button variant='outlined' color='primary' disableElevation onClick={() => reset()}>
            Cancel
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Form6;
