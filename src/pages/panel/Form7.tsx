import { yupResolver } from '@hookform/resolvers/yup';
import { Button, FormHelperText, Grid } from '@material-ui/core';
import { createStyles, fade, makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import { StyledToggleButton, StyledToggleButtonGroup } from 'components';
import { EditableTF } from 'components/editabletf';
import { Section } from 'components/section';
import { TextFieldNew } from 'components/textfield';
import { useAppState, useDispatch } from 'providers';
import React from 'react';
import { DragDropContext, Draggable, Droppable, DropResult } from 'react-beautiful-dnd';
import { Controller, useFieldArray, useForm } from 'react-hook-form';
import { getError, hasError } from 'utils';
import * as yup from 'yup';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    subtitle: {
      fontSize: 14,
      fontWeight: 'bold',
      marginTop: theme.spacing(2),
    },
    content: {
      //   marginTop: theme.spacing(3),
    },
    selected: {
      color: theme.palette.primary.main,
    },
    section: {
      marginTop: theme.spacing(3),
    },
    margin16: {
      marginTop: theme.spacing(1),
    },
    colorInput: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    padLeft16: {
      paddingLeft: theme.spacing(1),
    },
    actionsContainer: {
      marginTop: theme.spacing(3),
    },

    bordered: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
  }),
);

const initialValues = {
  messagesF7: [],
  visibilityF7: '',
  mediaF7: '',
  titleF7: '',
  textF7: '',
  confirmEmailF7: '',
  emailTextF7: '',
};

type StepType = typeof initialValues;

const stepValidationSchema = yup.object().shape({
  messagesF7: yup
    .array()
    .of(
      yup.object().shape({
        value: yup.string().required('This is required'),
      }),
    )
    // .min(1, 'At least 1 item(s) are required')
    .required('This is required'),
  visibilityF7: yup.string().required('This is required'),
  mediaF7: yup.string().required('This is required'),
  titleF7: yup.string().required('This is required'),
  textF7: yup.string().required('This is required'),
  confirmEmailF7: yup.string().required('This is required'),
  emailTextF7: yup.string().required('This is required'),
});

const Form7 = (): JSX.Element => {
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const { form7 } = useAppState();
  const defaultValues = { ...initialValues, ...form7 };
  const { control, register, errors, handleSubmit, reset } = useForm<StepType>({
    resolver: yupResolver(stepValidationSchema),
    defaultValues: defaultValues,
  });
  const { fields, append, remove, move } = useFieldArray({
    control, // control props comes from useForm (optional: if you are using FormContext)
    name: 'messagesF7', // unique name for your Field Array
  });

  const onDragged = (result: DropResult) => {
    const { destination, source } = result;
    if (!destination) return;
    if (destination.droppableId == source.droppableId && destination.index == source.index) return;
    move(source.index, destination.index);
  };

  return (
    <form
      className={classes.content}
      noValidate
      onSubmit={handleSubmit(data => {
        console.log(data);
        dispatch({ type: 'FORM7', payload: data });
      })}
    >
      <Section title heading='Message' subtitle='Allow visitors to send you a message.' />
      <Section className={classes.section} heading='Visiblity' subtitle='When should this tool be shown?' />

      <Grid container>
        <Grid item>
          <Controller
            control={control}
            name='visibilityF7'
            defaultValue={defaultValues.visibilityF7}
            render={({ value, ref, onBlur, onChange }) => (
              <StyledToggleButtonGroup
                className={classes.margin16}
                exclusive
                size='small'
                ref={ref}
                onBlur={onBlur}
                value={value}
                onChange={(e, val) => onChange(val)}
              >
                <StyledToggleButton value='always'>Always</StyledToggleButton>
                <StyledToggleButton value='workingHrs'>Working hours only</StyledToggleButton>
                <StyledToggleButton value='afterHrs'>After hours only</StyledToggleButton>
              </StyledToggleButtonGroup>
            )}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {errors.visibilityF7 && <FormHelperText error={errors.visibilityF7 ? true : false}>{errors.visibilityF7.message}</FormHelperText>}
        </Grid>
      </Grid>
      <Section className={classes.section} heading='Introduction media' subtitle='Add an image or video to the top of your form.' />
      <Grid container>
        <Grid item>
          <Controller
            control={control}
            name='mediaF7'
            defaultValue={defaultValues.mediaF7}
            render={({ value, ref, onBlur, onChange }) => (
              <StyledToggleButtonGroup
                className={classes.margin16}
                exclusive
                size='small'
                ref={ref}
                onBlur={onBlur}
                value={value}
                onChange={(e, val) => onChange(val)}
              >
                <StyledToggleButton value='imgOn'>On</StyledToggleButton>
                <StyledToggleButton value='imgOff'>Off</StyledToggleButton>
              </StyledToggleButtonGroup>
            )}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {errors.mediaF7 && <FormHelperText error={errors.mediaF7 ? true : false}>{errors.mediaF7.message}</FormHelperText>}
        </Grid>
      </Grid>
      <Section
        className={classes.section}
        heading='An introduction text'
        subtitle='This is the greeting text that sits at the top of the panel'
      />
      <Grid container spacing={2} className={classes.margin16}>
        <Grid item sm={12} xs={12} md={7} xl={7} lg={7}>
          <TextFieldNew
            inputRef={register}
            defaultValue={defaultValues.titleF7}
            id='titleF7'
            helperText={errors.titleF7 ? errors.titleF7.message : ''}
            error={errors.titleF7 ? true : false}
            fullWidth
            name='titleF7'
            label='Title'
            placeholder='Title example'
          />
        </Grid>
        <Grid item sm={12} xs={12} md={7} xl={7} lg={7}>
          <TextFieldNew
            fullWidth
            multiline
            inputRef={register}
            defaultValue={defaultValues.textF7}
            rows={5}
            id='textF7'
            helperText={errors.textF7 ? errors.textF7.message : ''}
            error={errors.textF7 ? true : false}
            label='Text'
            name='textF7'
            type='text'
            size='small'
            placeholder='I am an example of text that sits below the title'
          />
        </Grid>
      </Grid>

      <Section className={classes.section} heading='Build your message from' subtitle='Text explaining what this means' />

      <DragDropContext onDragEnd={onDragged}>
        <Droppable droppableId='droppable-7'>
          {provided => (
            <Grid container {...provided.droppableProps} ref={provided.innerRef}>
              {fields.map((field, index) => (
                <Draggable draggableId={`${field.id}`} index={index} key={field.id}>
                  {(provided, snapshot) => (
                    <Grid item md={7} sm={12} lg={7} xs={12} xl={7} {...provided.draggableProps} ref={provided.innerRef}>
                      <EditableTF
                        style={{ marginTop: theme.spacing(1) }}
                        isDragging={snapshot.isDragging}
                        dragHandleProps={provided.dragHandleProps}
                        text=''
                        key={field.id}
                        tfId={index}
                        tfRef={register()}
                        defaultValue={field.value}
                        name={`messagesF7[${index}].value`}
                        onDelete={() => {
                          remove(index);
                        }}
                      />
                    </Grid>
                  )}
                </Draggable>
              ))}
              {provided.placeholder}
            </Grid>
          )}
        </Droppable>
      </DragDropContext>
      <Grid container>
        <Grid item md={12} sm={12} xs={12} lg={12} xl={12}>
          {hasError(errors, 'messagesF7') && (
            <FormHelperText error={errors.messagesF7 ? true : false}>{getError(errors, 'messagesF7')}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Button
        variant='text'
        color='primary'
        disableElevation
        style={{ textTransform: 'none', fontWeight: 'bold', marginTop: 24 }}
        onClick={() => {
          append({ value: '' });
        }}
      >
        + Add new field
      </Button>

      <Section
        className={classes.section}
        heading='Confirm email'
        subtitle='This is the email sent to a user after they have successfully registered.'
      />
      <Grid container>
        <Grid item>
          <Controller
            control={control}
            name='confirmEmailF7'
            defaultValue={defaultValues.confirmEmailF7}
            render={({ value, ref, onBlur, onChange }) => (
              <StyledToggleButtonGroup
                className={classes.margin16}
                exclusive
                size='small'
                ref={ref}
                onBlur={onBlur}
                value={value}
                onChange={(e, val) => onChange(val)}
              >
                <StyledToggleButton value='imgOn'>On</StyledToggleButton>
                <StyledToggleButton value='imgOff'>Off</StyledToggleButton>
              </StyledToggleButtonGroup>
            )}
          />
        </Grid>
        <Grid item md={12} sm={12} xs={12} lg={12}>
          {errors.confirmEmailF7 && (
            <FormHelperText error={errors.confirmEmailF7 ? true : false}>{errors.confirmEmailF7.message}</FormHelperText>
          )}
        </Grid>
      </Grid>

      <Grid container>
        <Grid item sm={7} xs={12}>
          <TextFieldNew
            fullWidth
            multiline
            inputRef={register}
            defaultValue={defaultValues.emailTextF7}
            rows={5}
            id='emailTextF7'
            helperText={errors.emailTextF7 ? errors.emailTextF7.message : ''}
            error={errors.emailTextF7 ? true : false}
            label='Email text'
            name='emailTextF7'
            type='text'
            size='small'
            placeholder='Thank you for your message. We have  received your message and will respond to you the morning of the following business day.'
          />
        </Grid>
      </Grid>

      <Grid container className={classes.actionsContainer} spacing={1}>
        <Grid item>
          <Button type='submit' variant='contained' color='primary' disableElevation style={{ color: 'white' }}>
            Save
          </Button>
        </Grid>
        <Grid item>
          <Button variant='outlined' color='primary' disableElevation onClick={() => reset()}>
            Cancel
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Form7;
