import { Drawer, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { createStyles, fade, makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import { useAppState, useDispatch } from 'providers';
import React from 'react';
import Form1 from './Form1';
import Form2 from './Form2';
import Form3 from './Form3';
import Form4 from './Form4';
import Form5 from './Form5';
import Form6 from './Form6';
import Form7 from './Form7';

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: { display: 'flex' },
    // sidebar: { paddingTop: theme.spacing(3) },
    content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.paper,
      padding: theme.spacing(3),
      paddingLeft: theme.spacing(6),
    },
    title: {
      fontSize: 18,
      fontWeight: 'bold',
      color: theme.palette.text.primary,
    },
    subtitle: {
      fontSize: 15,
      marginTop: theme.spacing(2),
    },

    selected: {
      color: theme.palette.primary.main,
    },
    section: {
      marginTop: theme.spacing(3),
    },
    margin16: {
      marginTop: theme.spacing(1),
    },
    colorInput: {
      border: `1px solid ${fade(theme.palette.action.active, 0.12)}`,
      borderRadius: theme.shape.borderRadius,
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    padLeft16: {
      paddingLeft: theme.spacing(1),
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
  }),
);

const items = [
  {
    name: 'Hours of operation',
    id: 2,
    component: <Form3 />,
  },
  {
    name: 'Knowledge base',
    id: 4,
    component: <Form5 />,
  },
  {
    name: 'Style your panel',
    id: 0,
    component: <Form1 />,
  },
  {
    name: 'Request a call',
    id: 1,
    component: <Form2 />,
  },
  {
    name: 'Schedule a demo',
    id: 3,
    component: <Form4 />,
  },
  {
    name: 'Link',
    id: 5,
    component: <Form6 />,
  },
  {
    name: 'Message',
    id: 6,
    component: <Form7 />,
  },
];

const StylePanel = (): JSX.Element => {
  const classes = useStyles();
  const theme = useTheme();
  const dispatch = useDispatch();
  const { current, formStates } = useAppState();
  const handleListItemClick = (event: React.MouseEvent<HTMLDivElement, MouseEvent>, index: number) => {
    dispatch({ type: 'CURRENT', payload: index });
  };
  //   const index = `${current}`;
  return (
    <div className={classes.root}>
      <Drawer
        className={classes.drawer}
        variant='permanent'
        classes={{
          paper: classes.drawerPaper,
        }}
        PaperProps={{ style: { borderRight: 'none' } }}
        elevation={0}
        anchor='left'
      >
        <List component='nav' aria-label='main mailbox folders'>
          {formStates.map(item => (
            <ListItem
              style={{ borderTopRightRadius: theme.shape.borderRadius, borderBottomRightRadius: theme.shape.borderRadius }}
              button
              key={item.id}
              selected={current == item.id}
              onClick={event => handleListItemClick(event, item.id)}
            >
              <ListItemIcon>{item.submitted ? <CheckCircleIcon /> : ''}</ListItemIcon>
              <ListItemText primary={items[item.id].name} />
            </ListItem>
          ))}
        </List>
      </Drawer>
      <main className={classes.content}>{items[current].component}</main>
    </div>
  );
};

export default StylePanel;
