import { Drawer, Hidden } from '@material-ui/core/';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Step from '@material-ui/core/Step';
import StepContent from '@material-ui/core/StepContent';
import StepLabel from '@material-ui/core/StepLabel';
import Stepper from '@material-ui/core/Stepper';
import { createStyles, makeStyles, Theme, useTheme } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Step1 from 'pages/profile/Step1';
import Step2 from 'pages/profile/Step2';
import Step3 from 'pages/profile/Step3';
import React from 'react';

const drawerWidth = 300;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      width: '100%',
      display: 'flex',
    },
    content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      //   marginLeft: -drawerWidth,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    button: {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    actionsContainer: {
      marginBottom: theme.spacing(2),
    },
    resetContainer: {
      padding: theme.spacing(3),
    },
    margin: {
      marginTop: theme.spacing(3),
      marginBottom: theme.spacing(3),
    },
    imgBack: {
      width: drawerWidth,
      //   backgroundImage: 'url(/assets/leftimg1.png)',
      backgroundSize: 'contain',
      backgroundPosition: 'top',
      //   background: '#c31432' /* fallback for old browsers */,
      //   background: '-webkit-linear-gradient(to right, #240b36, #c31432)',
      background: 'linear-gradient(to bottom, #800080, #ffc0cb)',
    },
    selected: {
      backgroundColor: theme.palette.primary.light,
      borderColor: theme.palette.primary.main,
    },
    stepContent: {
      marginTop: theme.spacing(2),
      [theme.breakpoints.up('sm')]: {
        marginLeft: theme.spacing(2),
      },
    },
  }),
);

function getSteps() {
  return ['Sign Up', 'Message', 'Checkbox'];
}

export default function Profile(): JSX.Element {
  const classes = useStyles();
  const theme = useTheme();
  const [activeStep, setActiveStep] = React.useState(0);
  const [formData, setFormData] = React.useState({});

  const steps = getSteps();

  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  const handleSubmit = () => {
    console.log(formData);
  };

  const Steps = [
    <Step1 setFormData={setFormData} activeStep={activeStep} handleBack={handleBack} handleNext={handleNext} steps={3} key={0} />,
    <Step2 setFormData={setFormData} activeStep={activeStep} handleBack={handleBack} handleNext={handleNext} steps={3} key={1} />,
    <Step3 setFormData={setFormData} activeStep={activeStep} handleBack={handleBack} handleNext={handleNext} steps={3} key={2} />,
  ];

  return (
    <div className={classes.root}>
      <Hidden smDown implementation='css'>
        <Drawer
          className={classes.drawer}
          variant='permanent'
          classes={{
            paper: classes.imgBack,
          }}
          ModalProps={{
            keepMounted: true,
          }}
          anchor='left'
        />
      </Hidden>
      <div className={classes.content}>
        <div style={{ margin: theme.spacing(3) }}>
          <Typography variant='h5' color='textSecondary'>
            User Details
          </Typography>
        </div>
        <Stepper activeStep={activeStep} orientation='vertical'>
          {steps.map((label, index) => (
            <Step key={label}>
              <StepLabel>{label}</StepLabel>
              <StepContent TransitionProps={{ unmountOnExit: false }}>
                <div className={classes.stepContent}>{Steps[index % Steps.length]}</div>
              </StepContent>
            </Step>
          ))}
        </Stepper>
        {activeStep === steps.length && (
          <Paper square elevation={0} className={classes.resetContainer}>
            <Typography>All steps completed - you&apos;re finished</Typography>
            <Button variant='outlined' onClick={handleReset} className={classes.button}>
              Modify
            </Button>
            <Button variant='outlined' onClick={handleSubmit} className={classes.button}>
              Submit
            </Button>
          </Paper>
        )}
      </div>
    </div>
  );
}
