import { yupResolver } from '@hookform/resolvers/yup';
import { Grid } from '@material-ui/core/';
import Button from '@material-ui/core/Button';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { TextFieldNew } from 'components/textfield';
import React from 'react';
import { useForm } from 'react-hook-form';
import * as yup from 'yup';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    actionsContainer: {
      marginBottom: theme.spacing(2),
    },
  }),
);

const stepInitialValue = {
  firstname: '',
  lastname: '',
  dob: '',
  email: '',
  address: '',
};

type StepType = typeof stepInitialValue;

const stepValidationSchema = yup.object().shape({
  firstname: yup.string().required('First Name is Required'),
  lastname: yup.string().required('Last Name is Required'),
  dob: yup.string().required('DOB is required'),
  email: yup.string().email('Valid email is required').required(),
  address: yup.string().required(),
});

type Step = {
  activeStep: number;
  steps: number;
  handleBack(): void;
  handleNext(): void;
  // eslint-disable-next-line @typescript-eslint/ban-types
  setFormData: React.Dispatch<React.SetStateAction<{}>>;
};

// let renderCount = 0;

const Step1 = ({ activeStep, handleBack, handleNext, steps, setFormData }: Step): JSX.Element => {
  const classes = useStyles();

  const { register, handleSubmit, errors } = useForm<StepType>({
    resolver: yupResolver(stepValidationSchema),
  });

  return (
    <form
      noValidate
      onSubmit={handleSubmit(data => {
        // console.log(data);
        setFormData(val => {
          return { ...val, ...data };
        });
        handleNext();
      })}
    >
      <Grid container spacing={2}>
        <Grid item sm={6} xs={12}>
          <TextFieldNew
            helperText={errors.firstname ? errors.firstname.message : ''}
            fullWidth
            error={errors.firstname ? true : false}
            label='First Name'
            name='firstname'
            id='firstname'
            type='text'
            size='small'
            placeholder='Tony'
            inputRef={register}
            defaultValue=''
          />
        </Grid>
        <Grid item sm={6} xs={12}>
          <TextFieldNew
            helperText={errors.lastname ? errors.lastname.message : ''}
            error={errors.lastname ? true : false}
            fullWidth
            inputRef={register}
            defaultValue=''
            name='lastname'
            id='lastname'
            label='Last Name'
            type='text'
            size='small'
            placeholder='Stark'
          />
        </Grid>
        <Grid item sm={6} xs={12}>
          <TextFieldNew
            fullWidth
            helperText={errors.dob ? errors.dob.message : ''}
            error={errors.dob ? true : false}
            inputRef={register}
            defaultValue=''
            name='dob'
            id='dob'
            size='small'
            label='Date of Birth'
            type='date'
            placeholder='2021-02-01'
          />
        </Grid>
        <Grid item sm={6} xs={12}>
          <TextFieldNew
            fullWidth
            helperText={errors.email ? errors.email.message : ''}
            error={errors.email ? true : false}
            inputRef={register}
            defaultValue=''
            name='email'
            label='Email Address'
            type='email'
            id='email'
            size='small'
            placeholder='username@mail.com'
          />
        </Grid>
        <Grid item sm={12} xs={12}>
          <TextFieldNew
            fullWidth
            helperText={errors.address ? errors.address.message : ''}
            error={errors.address ? true : false}
            placeholder='House no. 123, Lane 2'
            inputRef={register}
            defaultValue=''
            name='address'
            label='Address'
            type='text'
            id='address'
            size='small'
          />
        </Grid>
        <Grid item className={classes.actionsContainer} sm={12} container>
          <Grid item>
            <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
              Back
            </Button>
          </Grid>
          <Grid item>
            <Button variant='contained' color='primary' type='submit' className={classes.button}>
              {activeStep === steps - 1 ? 'Finish' : 'Next'}
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
};

export default Step1;
