import { yupResolver } from '@hookform/resolvers/yup';
import { FormControlLabel, FormHelperText, Grid, RadioGroup } from '@material-ui/core/';
import Button from '@material-ui/core/Button';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { StyledRadio } from 'components/radio';
import { TextFieldNew } from 'components/textfield';
import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    actionsContainer: {
      marginBottom: theme.spacing(2),
    },
  }),
);

const stepInitialValue = {
  message: '',
  step2Choice: '',
};

type StepType = typeof stepInitialValue;

const stepValidationSchema = yup.object().shape({
  message: yup.string().required('Message is Required'),
  step2Choice: yup.string().required('Please select one of these'),
});

type Step = {
  activeStep: number;
  steps: number;
  handleBack(): void;
  handleNext(): void;

  // eslint-disable-next-line @typescript-eslint/ban-types
  setFormData: React.Dispatch<React.SetStateAction<{}>>;
};

const Step2 = ({ activeStep, handleBack, handleNext, steps, setFormData }: Step): JSX.Element => {
  const classes = useStyles();

  const { register, handleSubmit, errors, control } = useForm<StepType>({
    resolver: yupResolver(stepValidationSchema),
  });

  return (
    <form
      noValidate
      onSubmit={handleSubmit(data => {
        // console.log(data);
        setFormData(val => {
          return { ...val, ...data };
        });
        handleNext();
      })}
    >
      <Grid container spacing={2}>
        <Grid item sm={12} xs={12}>
          <TextFieldNew
            fullWidth
            multiline
            inputRef={register}
            defaultValue=''
            rows={10}
            id='message'
            helperText={errors.message ? errors.message.message : ''}
            error={errors.message ? true : false}
            label='Message'
            name='message'
            type='text'
            size='small'
            placeholder=''
          />
        </Grid>
        <Grid item sm={12} container>
          <Grid item>
            <Controller
              control={control}
              name='step2Choice'
              defaultValue=''
              as={
                <RadioGroup row defaultValue='' aria-label='radio-buttons'>
                  <FormControlLabel value='one' control={<StyledRadio />} label='The number one choice' />

                  <FormControlLabel value='two' control={<StyledRadio />} label='The number two choice' />
                </RadioGroup>
              }
            />
          </Grid>
          <Grid item sm={12}>
            {errors.step2Choice ? <FormHelperText error>{errors.step2Choice.message}</FormHelperText> : ''}
          </Grid>
        </Grid>
        <Grid item className={classes.actionsContainer} sm={12} container>
          <Grid item>
            <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
              Back
            </Button>
          </Grid>
          <Grid item>
            <Button variant='contained' color='primary' type='submit' className={classes.button}>
              {activeStep === steps - 1 ? 'Finish' : 'Next'}
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
};

export default Step2;
