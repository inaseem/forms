import { yupResolver } from '@hookform/resolvers/yup';
import { FormControlLabel, FormHelperText, Grid, RadioGroup } from '@material-ui/core/';
import Button from '@material-ui/core/Button';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import { AcUnit, Atm } from '@material-ui/icons/';
import { StyledRadio } from 'components/radio';
import React from 'react';
import { Controller, useForm } from 'react-hook-form';
import * as yup from 'yup';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    button: {
      marginTop: theme.spacing(1),
      marginRight: theme.spacing(1),
    },
    actionsContainer: {
      marginBottom: theme.spacing(2),
    },
    selected: {
      backgroundColor: theme.palette.primary.light,
      borderColor: theme.palette.primary.main,
    },
  }),
);

const stepInitialValue = {
  step3Choice: '',
  check1: false,
  check2: false,
};

type StepType = typeof stepInitialValue;

const stepValidationSchema = yup.object().shape({
  step3Choice: yup.string().required('Select one of the above'),
});

type Step = {
  activeStep: number;
  steps: number;
  handleBack(): void;
  handleNext(): void;

  // eslint-disable-next-line @typescript-eslint/ban-types
  setFormData: React.Dispatch<React.SetStateAction<{}>>;
};

const Step3 = ({ activeStep, handleBack, handleNext, steps, setFormData }: Step): JSX.Element => {
  const classes = useStyles();

  const { register, unregister, handleSubmit, errors, setValue, control, watch } = useForm<StepType>({
    resolver: yupResolver(stepValidationSchema),
  });

  React.useEffect(() => {
    register('check1');
    register('check2');
    setValue('check1', false);
    setValue('check2', false);
    return () => unregister(['check1', 'check2']);
  }, []);

  const check1 = watch('check1', false);
  const check2 = watch('check2', false);

  return (
    <form
      noValidate
      onSubmit={handleSubmit(data => {
        // console.log(data);
        setFormData(val => {
          return { ...val, ...data };
        });
        handleNext();
      })}
    >
      <Grid container spacing={2}>
        <Grid item sm={12} container spacing={4} xs={12}>
          <Grid item sm={6} xs={6}>
            <Button
              variant='outlined'
              size='large'
              onClick={() => setValue('check1', !check1)}
              fullWidth
              style={{ height: 150 }}
              className={check1 ? classes.selected : ''}
            >
              <AcUnit fontSize='large' />
            </Button>
          </Grid>
          <Grid item sm={6} xs={6}>
            <Button
              variant='outlined'
              size='large'
              onClick={() => setValue('check2', !check2)}
              fullWidth
              style={{ height: 150 }}
              className={check2 ? classes.selected : ''}
            >
              <Atm fontSize='large' />
            </Button>
          </Grid>
        </Grid>
        <Grid item sm={12} container>
          <Grid item>
            <Controller
              control={control}
              name='step3Choice'
              defaultValue=''
              as={
                <RadioGroup defaultValue='' aria-label='radio buttons'>
                  <FormControlLabel value='one' control={<StyledRadio />} label='I want to add this option' />

                  <FormControlLabel
                    value='two'
                    control={<StyledRadio />}
                    label='Let me check on this checkbox and choose some cool stuff'
                  />
                </RadioGroup>
              }
            />
          </Grid>
          <Grid item sm={12}>
            {errors.step3Choice ? <FormHelperText error>{errors.step3Choice.message}</FormHelperText> : ''}
          </Grid>
        </Grid>
        <Grid item className={classes.actionsContainer} sm={12} container>
          <Grid item>
            <Button disabled={activeStep === 0} onClick={handleBack} className={classes.button}>
              Back
            </Button>
          </Grid>
          <Grid item>
            <Button variant='contained' color='primary' type='submit' className={classes.button}>
              {activeStep === steps - 1 ? 'Finish' : 'Next'}
            </Button>
          </Grid>
        </Grid>
      </Grid>
    </form>
  );
};

export default Step3;
