import { useReducer } from 'react';
import { createContainer } from 'react-tracked';

/* eslint-disable  @typescript-eslint/no-explicit-any */

const initialState = {
  form1: {},
  form2: {},
  form3: {},
  form4: {},
  form5: {},
  form6: {},
  form7: {},
  current: 0,
  formStates: [
    {
      id: 0,
      submitted: false,
      active: true,
    },
    {
      id: 1,
      submitted: false,
      active: false,
    },
    {
      id: 2,
      submitted: false,
      active: false,
    },
    { id: 3, submitted: false, active: false },
    { id: 4, submitted: false, active: false },
    { id: 5, submitted: false, active: false },
    { id: 6, submitted: false, active: false },
  ],
  total: 7,
};

type State = typeof initialState;

export type Action =
  | { type: 'FORM1'; payload: any }
  | { type: 'FORM2'; payload: any }
  | { type: 'FORM3'; payload: any }
  | { type: 'FORM4'; payload: any }
  | { type: 'FORM5'; payload: any }
  | { type: 'FORM6'; payload: any }
  | { type: 'FORM7'; payload: any }
  | { type: 'CURRENT'; payload: number }
  | { type: 'NEXT' };

const reducer = (state: State, action: Action): State => {
  switch (action.type) {
    case 'FORM1':
      return {
        ...state,
        form1: { ...state.form1, ...action.payload },
        // formStates: [ ...state.formStates,  ],
        formStates: state.formStates.map(form => (form.id === 0 ? { ...form, submitted: true } : form)),
      };
    case 'FORM2':
      return {
        ...state,
        form2: { ...state.form2, ...action.payload },
        formStates: state.formStates.map(form => (form.id === 1 ? { ...form, submitted: true } : form)),
      };
    case 'FORM3':
      return {
        ...state,
        form3: { ...state.form3, ...action.payload },
        formStates: state.formStates.map(form => (form.id === 2 ? { ...form, submitted: true } : form)),
      };
    case 'FORM4':
      return {
        ...state,
        form4: { ...state.form4, ...action.payload },
        formStates: state.formStates.map(form => (form.id === 3 ? { ...form, submitted: true } : form)),
      };
    case 'FORM5':
      return {
        ...state,
        form5: { ...state.form5, ...action.payload },
        formStates: state.formStates.map(form => (form.id === 4 ? { ...form, submitted: true } : form)),
      };
    case 'FORM6':
      return {
        ...state,
        form6: { ...state.form6, ...action.payload },
        formStates: state.formStates.map(form => (form.id === 5 ? { ...form, submitted: true } : form)),
      };
    case 'FORM7':
      return {
        ...state,
        form7: { ...state.form7, ...action.payload },
        formStates: state.formStates.map(form => (form.id === 6 ? { ...form, submitted: true } : form)),
      };
    case 'CURRENT':
      return {
        ...state,
        current: action.payload,
        formStates: state.formStates.map(form => (form.id === action.payload ? { ...form, active: true } : { ...form, active: false })),
      };
    case 'NEXT':
      const index = (state.current + 1) % state.total;
      return {
        ...state,
        current: index,
        formStates: state.formStates.map(form => (form.id === index ? { ...form, active: true } : { ...form, active: false })),
      };
    default:
      return state;
  }
};
const useValue = () => useReducer(reducer, initialState);

export const { Provider, useTrackedState: useAppState, useUpdate: useDispatch, useSelector } = createContainer(useValue);
