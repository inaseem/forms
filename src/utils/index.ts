/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable  @typescript-eslint/no-explicit-any */

export const hasError = (data: any, prop: string): boolean => {
  if (!data) return false;
  if (!data[prop]) return false;
  if (!data[prop]?.message) return false;
  return true;
};

export const getError = (data: any, prop: string): string => {
  if (!data) return '';
  if (!data[prop]) return '';
  if (!data[prop].message) return '';
  return data[prop].message;
};
